<?php

namespace Src\TableGateways;

class MagasinGateway
{

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findAll()
    {
        $statement = "
            SELECT 
                id, nom, telephone, email, adresse, ville, code_cp
            FROM
                magasin;
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function find($id)
    {
        $statement = "
            SELECT 
                id, nom, telephone, email, adresse, ville, code_cp
            FROM
                magasin
            WHERE id = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function insert(array $input)
    {
        $statement = "
            INSERT INTO magasin 
                (nom, telephone, email, adresse, ville, code_cp)
            VALUES
                (:nom, :telephone, :email, :adresse, :ville, :code_cp);
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'nom' => $input['nom'],
                'telephone' => $input['telephone'],
                'email' => $input['email'],
                'adresse' => $input['adresse'],
                'ville' => $input['ville'],
                'code_cp' => $input['code_cp'],
            ));
            return $this->db->lastInsertId();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function update($id, array $input)
    {
        $statement = "
            UPDATE magasin
            SET 
                nom = :nom,
                telephone = :telephone,
                email = :email,
                adresse = :adresse,
                ville = :ville,
                code_cp = :code_cp
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'id' => (int) $id,
                'nom' => $input['nom'],
                'telephone' => $input['telephone'],
                'email' => $input['email'],
                'adresse' => $input['adresse'],
                'ville' => $input['ville'],
                'code_cp' => $input['code_cp'],
            ));
            return json_encode(array("id" => $id) + $input);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function delete($id)
    {
        $statement = "
            DELETE FROM magasin
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array('id' => $id));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}
