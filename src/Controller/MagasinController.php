<?php

namespace Src\Controller;

use Src\TableGateways\MagasinGateway;

class MagasinController
{

    private $db;
    private $requestMethod;
    private $magasinId;

    private $magasinGateway;

    public function __construct($db, $requestMethod, $magasinId)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->magasinId = $magasinId;

        $this->magasinGateway = new MagasinGateway($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->magasinId) {
                    $response = $this->getMagasin($this->magasinId);
                } else {
                    $response = $this->getAllMagasins();
                };
                break;
            case 'POST':
                $response = $this->createMagasinFromRequest();
                break;
            case 'PUT':
                $response = $this->updateMagasinFromRequest($this->magasinId);
                break;
            case 'DELETE':
                $response = $this->deleteMagasin($this->magasinId);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllMagasins()
    {
        $result = $this->magasinGateway->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function getMagasin($id)
    {
        $result = $this->magasinGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function createMagasinFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), true);
        if (! $this->validateMagasin($input)) {
            return $this->unprocessableEntityResponse();
        }
        $id_nserted = $this->magasinGateway->insert($input);
        $input = array("id" => $id_nserted) + $input;
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = json_encode($input);
        return $response;
    }

    private function updateMagasinFromRequest($id)
    {
        $result = $this->magasinGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $input = (array) json_decode(file_get_contents('php://input'), true);
        if (! $this->validateMagasin($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->magasinGateway->update($id, $input);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode(array("id" => $id) + $input);
        return $response;
    }

    private function deleteMagasin($id)
    {
        $result = $this->magasinGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->magasinGateway->delete($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode(array("success" => "deleted successfully"));
        return $response;
    }

    private function validateMagasin($input)
    {
        if (
            !isset($input['nom'])
            || ! isset($input['telephone'])
            || ! isset($input['email'])
            || ! isset($input['adresse'])
            || ! isset($input['ville'])
            || ! isset($input['code_cp'])
        ) {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}
