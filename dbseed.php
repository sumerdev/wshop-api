<?php
//require 'bootstrap.php';

$servername = "mysql";
$username = "root";
$password = "mypass";
$dbname = "api_wshop";

try {
    $dbh = new PDO('mysql:host=mysql;dbname=api_wshop', $username, $password);

    $statement = <<<EOS
        DROP TABLE IF EXISTS `magasin`;
        CREATE TABLE IF NOT EXISTS `magasin` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `nom` varchar(255) NOT NULL,
        `telephone` varchar(255) NOT NULL,
        `email` varchar(255) NOT NULL,
        `adresse` varchar(255) NOT NULL,
        `ville` varchar(255) NOT NULL,
        `code_cp` varchar(255) NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
        INSERT INTO `magasin` (`id`, `nom`, `telephone`, `email`, `adresse`, `ville`, `code_cp`) VALUES
        (1, 'Ipsum Consulting', '04 95 28 09 69', 'eget.odio.Aliquam@Quisque.co.uk', 'Ap #441-2917 Nisi Rd.', 'Macerata', '20209'),
        (2, 'Et Magna Institute', '01 74 00 52 24', 'sed.sapien@rutrumFusce.net', '372-8756 Etiam Rd.', 'Radebeul', '55636'),
        (3, 'Nullam Nisl Incorporated', '09 19 34 85 30', 'Quisque.nonummy@dolor.org', '176-3897 Gravida St.', 'Rio Verde', '91075'),
        (4, 'Nullam Incorporated', '06 19 77 24 44', 'ullamcorper@velarcu.com', '194-5720 Etiam Road', 'Westmeerbeek', '78936'),
        (5, 'Maecenas Malesuada Industries', '03 94 55 67 37', 'commodo@semperpretiumneque.ca', '236-4579 Eget Street', 'Morena', '07581'),
        (6, 'Semper Egestas Ltd', '06 96 75 76 84', 'diam.eu.dolor@consectetueradipiscingelit.com', '208-3452 Tellus Rd.', 'Lévis', '11354'),
        (7, 'Libero Mauris Institute', '02 89 63 78 61', 'feugiat.Lorem@Maurismolestiepharetra.org', '7791 Enim, St.', 'Bungay', '36618'),
        (8, 'Vitae Posuere LLP', '09 37 62 53 74', 'consequat@cursusin.ca', '357 Mollis. St.', 'Murdochville', '52432'),
        (9, 'Nullam Feugiat Placerat Consulting', '05 66 20 97 71', 'dui@ultricies.net', '5857 Sit Ave', 'Grimsby', '62174'),
        (10, 'Ipsum Consulting', '04 95 28 09 69', 'eget.odio.Aliquam@Quisque.co.uk', 'Ap #441-2917 Nisi Rd.', 'Macerata', '20209'),
        (11, 'Et Magna Institute', '01 74 00 52 24', 'sed.sapien@rutrumFusce.net', '372-8756 Etiam Rd.', 'Radebeul', '55636'),
        (12, 'Nullam Nisl Incorporated', '09 19 34 85 30', 'Quisque.nonummy@dolor.org', '176-3897 Gravida St.', 'Rio Verde', '91075'),
        (13, 'Nullam Incorporated', '06 19 77 24 44', 'ullamcorper@velarcu.com', '194-5720 Etiam Road', 'Westmeerbeek', '78936'),
        (14, 'Maecenas Malesuada Industries', '03 94 55 67 37', 'commodo@semperpretiumneque.ca', '236-4579 Eget Street', 'Morena', '07581'),
        (15, 'Semper Egestas Ltd', '06 96 75 76 84', 'diam.eu.dolor@consectetueradipiscingelit.com', '208-3452 Tellus Rd.', 'Lévis', '11354');

    EOS;
    $dbh->query($statement);
    echo "New record created successfully";

} catch (PDOException $e) {
    echo "Erreur !: " . $e->getMessage();
}